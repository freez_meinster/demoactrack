from __future__ import unicode_literals

from django.apps import AppConfig

class DemoConfig(AppConfig):
    name = 'demo'

    def ready(self):
        from django.contrib.auth.models import User
        import actrack

        actrack.connect(User)
