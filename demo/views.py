from django.shortcuts import render
from django.http import HttpResponse
import actrack

def landing(request):
    actrack.log(request.user, "visit_landing")
    return HttpResponse("OK")

def report(request):
    rep = request.user.actions.all()
    return HttpResponse([x.actor for x in rep])
